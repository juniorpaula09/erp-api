CREATE DATABASE IF NOT EXISTS erpsystem;
USE erpsystem;

CREATE TABLE products(
    id int auto_increment primary key,
    product_cod int not null,
    name varchar(50) not null,
    price float not null,
    description varchar(255),
    created_at timestamp default current_timestamp()
)ENGINE=INNODB;