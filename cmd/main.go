package main

import (
	"erp/internal/config"
	"erp/internal/router"
	"fmt"
	"log"
	"net/http"
)

func main() {
	config.InitVariables()
	fmt.Println("server started on port:", config.Port)

	r := router.HanlderRoutes()

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.Port), r))
}
