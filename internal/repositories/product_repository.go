package repositories

import (
	"database/sql"
	"erp/internal/models"
)

type Product struct {
	Db *sql.DB
}

func NewProductRepository(db *sql.DB) *Product {
	return &Product{db}
}

func (r Product) Create(product models.Product) (uint64, error) {
	stmt, err := r.Db.Prepare(
		"insert into products (product_cod, name, price, description) values (?,?,?,?)",
	)
	if err != nil {
		return 0, nil
	}
	defer stmt.Close()

	result, err := stmt.Exec(product.Cod, product.Name, product.Price, product.Description)
	if err != nil {
		return 0, nil
	}

	insertedID, err := result.LastInsertId()
	if err != nil {
		return 0, nil
	}

	return uint64(insertedID), nil
}

func (r Product) Find() ([]models.Product, error) {
	rows, err := r.Db.Query(`
		select * from products order by id desc
	`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var products []models.Product

	for rows.Next() {
		var product models.Product

		if err = rows.Scan(
			&product.ID,
			&product.Cod,
			&product.Name,
			&product.Price,
			&product.Description,
			&product.CreatedAt,
		); err != nil {
			return nil, err
		}

		products = append(products, product)
	}

	return products, nil
}

func (r Product) FindByID(productID uint64) (models.Product, error) {
	row, err := r.Db.Query("select * from products where id = ?", productID)
	if err != nil {
		return models.Product{}, err
	}
	defer row.Close()

	var product models.Product

	if row.Next() {
		if err = row.Scan(
			&product.ID,
			&product.Cod,
			&product.Name,
			&product.Price,
			&product.Description,
			&product.CreatedAt,
		); err != nil {
			return models.Product{}, err
		}
	}

	return product, nil
}

func (r Product) Update(productID uint64, product models.Product) error {
	stmt, err := r.Db.Prepare("update products set product_cod = ?, name = ?, price = ?, description = ? where id = ?")
	if err != nil {
		return err
	}
	defer stmt.Close()

	if _, err = stmt.Exec(product.Cod, product.Name, product.Price, product.Description, productID); err != nil {
		return err
	}
	return nil
}

func (r Product) Delete(productID uint64) error {
	stmt, err := r.Db.Prepare("delete from products where id = ?")
	if err != nil {
		return err
	}
	defer stmt.Close()

	if _, err = stmt.Exec(productID); err != nil {
		return err
	}

	return nil
}
