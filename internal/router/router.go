package router

import (
	"erp/internal/router/routes"

	"github.com/gorilla/mux"
)

func HanlderRoutes() *mux.Router {
	r := mux.NewRouter()
	return routes.SetupRoutes(r)
}
