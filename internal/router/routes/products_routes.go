package routes

import (
	"erp/internal/controllers"
	"net/http"
)

var ProductsRoutes = []Route{
	{
		URI:      "/products",
		Method:   http.MethodPost,
		Function: controllers.CreateProduct,
		HasAuth:  false,
	},
	{
		URI:      "/products",
		Method:   http.MethodGet,
		Function: controllers.FindProducts,
		HasAuth:  false,
	},
	{
		URI:      "/products/{productId}",
		Method:   http.MethodGet,
		Function: controllers.FindProductByID,
		HasAuth:  false,
	},
	{
		URI:      "/products/{productId}",
		Method:   http.MethodPut,
		Function: controllers.UpdateProduct,
		HasAuth:  false,
	},
	{
		URI:      "/products/{productId}",
		Method:   http.MethodDelete,
		Function: controllers.DeleteProduct,
		HasAuth:  false,
	},
}
