package models

import (
	"errors"
	"time"
)

type Product struct {
	ID          uint64    `json:"id,omitempty"`
	Cod         int       `json:"product_code"`
	Name        string    `json:"name"`
	Price       float64   `json:"price"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at,omitempty"`
}

func (p *Product) Validation() error {
	if err := p.validate(); err != nil {
		return err
	}

	return nil
}

func (p *Product) validate() error {
	if p.Cod == 0 {
		return errors.New("cod is required")
	}
	if p.Price == 0 {
		return errors.New("price is required")
	}
	if p.Name == "" {
		return errors.New("name is required")
	}
	if p.Description == "" {
		return errors.New("price is required")
	}
	if len(p.Description) > 255 {
		return errors.New("description is must large")
	}

	return nil
}
